from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from accounts.forms import AccountForm, SignupForm
from django.contrib.auth.models import User 
from django.conf import settings


# Create your views here.
# LoginView
def user_login(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # Authenticating users -> Refer to Django doc
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                # If user is registered, redirect home
                return redirect("home")
            # else:
            #     # else redirect to login page
            #     return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)

# Log user out 
# How to log a user out 
def user_logout(request): 
    logout(request)
    return redirect("login")


def create_user(request): 
    if request.method == "POST": 
        form = SignupForm(request.POST)
        if form.is_valid(): 
            username = form.cleaned_data["username"] 
            password = form.cleaned_data["password"] 
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation: 
                # Create new user account from their username and password 
                user = User.objects.create_user(username, password=password)
                # login the user
                login(request, user) 
                return redirect("home")
            else: 
                form.add_error("password", "the passwords do not match")
    else:
        form = SignupForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
        