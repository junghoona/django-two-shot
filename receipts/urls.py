from django.urls import path
from receipts.views import (
    show_receipts,
    create_receipt,
    show_accounts,
    show_expense_category,
    create_category,
    create_account,
)


# Add url param : id
# Register url patterns for receipts view functions here
urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expense_category, name="category_list"),
    path("accounts/", show_accounts, name="show_accounts"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
