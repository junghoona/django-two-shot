from django.shortcuts import render, redirect 
from receipts.models import Receipt, ExpenseCategory, Account 
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create your views here.
# ReceiptList View
@login_required
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        # Refer to this receipt list with this key in html
        "receipts": receipt_list
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
# On a successful creation of a receipt, redirect the browser to the "home"
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def show_expense_category(request):
    expense_category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_category": expense_category,
    }
    return render(request, "categories/list.html", context)


@login_required
def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            # Set owner to current user
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            # Set owner to current user
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("show_accounts")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
